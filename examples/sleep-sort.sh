#!/bin/bash

for val in "$@"
do
	if ! [ "$val" -eq "$val" ] 2>/dev/null; then echo numeric arguments only >&2 ; exit 1; fi
	(sleep $val; echo $val) &
done

wait
