\DeclareOption{czech}{
\def\lang{1}
}

\ExecuteOptions{oneside}
\ProcessOptions

\LoadClass{article}

\usepackage{fancyhdr}
\usepackage{fullpage}

\if\lang 1{%
% \usepackage{polyglossia}
% \setdefaultlanguage{czech}
% \AtBeginDocument{}
}\else{}
\fi

\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{Ahoj \thepage}
\fancyfoot[L]{Muj dokument}
% \re{\headrulewidth}{0pt}

% \usepackage{geometry}
% \geometry{margins=4cm}
\setlength{\itemsep}{0pt}
\setlength{\parindent}{0pt}

\newenvironment{dukaz}{\textbf{Dukaz:}}{ctverecek}
