Workshop LaTeX pokročilí
========================

Lokální použití LaTeX
---------------------

### Instalace

- https://tug.org/ odkazy na TeX Live, MacTeX a proTeXt
- IDE: https://apps.kde.org/kile/ a https://xm1math.net/texmaker/

### Spuštění z příkazové řádky

1. `pdflatex hello.tex`, projít výpis, ukázat všechny pomocné soubory
2. `pdflatex figure.tex`
   - ukázat `figure.pdf` a zdůraznit otazníky
   - STUDENTI: co asi tak obsahuje `figure.aux`?
   - STUDENTI: zkuste z výpisu na konzoli (nebo `figure.log`) zjistit, co je špatně a co máme dělat
3. `pdflatex citation.tex`
   1. STUDENTI: co máme podle výpisu dělat
   2. `biber citation` (zmíníme že existuje ještě `bibtex`)
   3. doplníme příponu `.bib`
   4. projít `citation.bbl`
   5. `pdflatex citation.tex`; STUDENTI co teď?
4. `pdflatex pygments-src.tex`
   1. STUDENTI: co teď?
   1. zkusit `H`
   2. zkusit `?`
   3. zkusit `X`
   4. zkusit `pdflatex -interaction=nonstopmode -halt-on-error pygments-src.tex`
   4. zkusit `pdflatex -shell-escape pygments-src.tex`
   5. ukázat `pygments-src.pdf`

### Engines

1. `latex hello.tex`
   - výstupní soubory, prohlednout `hello.dvi`
   - `dvipdf hello.dvi`
   - ukázat existenci dalších konvertorů
2. `xelatex hello.tex`
3. `lualatex hello.tex`

### Jednodušší volání

1. `latexmk -pdf figure.tex`
2. `latexmk -xelatex citation.tex`
3. `latexmk -lualatex -shell-escape pygments-src.tex`


Vlastní balíček a šablona
---------------

### Vlastní šablona

1. založíme `fitreport.cls` a `myreport.tex`
2. do `fitreport.cls` vložit `\LoadClass{article}` a zmínit přehled dalších tříd: základní, https://ctan.org/topic/class
- fancyhdr
- geometry
- newcommand, newenvironment
- declareoption
- AtBeginDocument


### Vlastní balíček

kdy balíček a kdy šablona
